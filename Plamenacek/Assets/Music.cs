﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public AudioClip[] clips;
    public AudioSource audio;
    private float countdown = 10f;
    private int clipIndex = 0;
    void Start()
    {
       // audio = GetComponent<AudioSource>();
    }
    
    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0)
        {
            audio.clip = clips[clipIndex];
            audio.Play();
            countdown = 300f;
            clipIndex++;
            if (clipIndex == 3)
                clipIndex = 0;
        }
    }
}
