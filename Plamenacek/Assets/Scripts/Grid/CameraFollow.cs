﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject placeholder;
    public static bool col = false;
    public GameObject building;

    private GameObject Event;
   

   private Grid grid;

   private void Awake()
   {
       grid = FindObjectOfType<Grid>();
       Event = GameObject.FindGameObjectWithTag("EventSystem");
   }

   private void Update()
   {
       if (Input.GetMouseButtonDown(0) && col == false)
       {
           RaycastHit hitInfo;
           Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
           if (Physics.Raycast(ray, out hitInfo))
           {
               PlaceCubeNear(hitInfo.point);
           }
           Debug.Log(col);
       }
       else
       {
           RaycastHit hitInfo;
           Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
           if (Physics.Raycast(ray, out hitInfo))
           {
               Vector3 a = grid.GetNearestPointOnGrid(hitInfo.point);
               var finalPosition = new Vector3(a.x, 0.5f, a.z);
               Debug.Log(finalPosition + " dawdawd");
               placeholder.transform.position = finalPosition;
               
           }
       }
   }

   private void PlaceCubeNear(Vector3 clickPoint)
   {
       
       var finalPosition = grid.GetNearestPointOnGrid(clickPoint);
       GameObject.Instantiate(building, new Vector3(finalPosition.x, 0.5f, finalPosition.z), Quaternion.identity);
       this.enabled = false;
       AstarPath.active.Scan();

   }
   
   
}

