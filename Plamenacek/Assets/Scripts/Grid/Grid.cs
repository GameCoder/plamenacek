﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
   /*public GameObject target;
   public GameObject floor;
   private Vector3 pos;
   public float gridSize;

   private void LateUpdate()
   {
      pos.x = Mathf.Floor(target.transform.position.x / gridSize) * gridSize;
      pos.y = Mathf.Floor(target.transform.position.y / gridSize) * gridSize;
      pos.z = Mathf.Floor(target.transform.position.z / gridSize) * gridSize;

      floor.transform.position = pos;
   }
   public bool snapToGrid = true;
   public float snapValue = 0.5f;
 
   public bool sizeToGrid = false;
   public float sizeValue = 0.25f;
   void Update ()
   {
      if (snapToGrid)
         transform.position = RoundTransform (transform.position, snapValue);
 
      if (sizeToGrid)
         transform.localScale = RoundTransform(transform.localScale, sizeValue);

      
   }

   void OnGUI()
   {
      Event currentEvent = Event.current;
      if (currentEvent.isMouse)
      {
         Vector2 mouseXY = new Vector2(currentEvent.mousePosition.x - Screen.width / 2,
            currentEvent.mousePosition.y - Screen.height / 2);
         Debug.Log(mouseXY);
      }
   }

   // The snapping code
   private Vector3 RoundTransform (Vector3 v, float snapValue)
   {
      return new Vector3
      (
         snapValue * Mathf.Round(v.x / snapValue),
         v.y,
         snapValue * Mathf.Round(v.z / snapValue)
      );
   }*/
   [SerializeField]
   private float size = 1f;

   public Vector3 GetNearestPointOnGrid(Vector3 position)
   {
      position -= transform.position;

      int xCount = Mathf.RoundToInt(position.x / size);
      int yCount = Mathf.RoundToInt(position.y / size);
      int zCount = Mathf.RoundToInt(position.z / size);

      Vector3 result = new Vector3(
         (float)xCount * size,
         (float)yCount * size,
         (float)zCount * size);

      result += transform.position;

      return result;
   }

   private void OnDrawGizmos()
   {
      Gizmos.color = Color.yellow;
      for (float x = -40; x < 40; x += size)
      {
         for (float z = -40; z < 40; z += size)
         {
            var point = GetNearestPointOnGrid(new Vector3(x, 0f, z));
            Gizmos.DrawSphere(point, 0.1f);
         }
                
      }
   }
}
