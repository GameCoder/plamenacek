﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class UnitPath : MonoBehaviour
{
    private Unit unit;
    private Seeker seeker;
    private CharacterController controller;
    public GameObject[] obj;
    public Path path;
    private Animator anim;
    private Vector3 animPos;
    public float speed;
    private AudioManager audio;

    public float nextWaypointDistance = 10f;
    private int currentWaypoint = 0;

    void Start()
    {
        seeker = GetComponent<Seeker>();
        controller = GetComponent<CharacterController>();
        unit = GetComponent<Unit>();
        GameObject eventSystem = GameObject.FindWithTag("EventSystem");
        audio = eventSystem.GetComponent<AudioManager>();
        anim = GetComponent<Animator>();

    }

    public void LateUpdate()
    {
        if (unit.selected)
        {
            
            if (Input.GetMouseButtonDown(1) )
            {
                anim.SetBool("isWalking", true);
                seeker.StartPath(transform.position, MouseSelection.rightClickPosition, _onPathComplete);
                animPos = MouseSelection.rightClickPosition;
                animPos.y = 0.0f;
                transform.LookAt(animPos);
                if (unit.unitType.unitType == "Worker")
                {
                    if (UnityEngine.Random.Range(1,3) != 1)
                     audio.PlaySound(UnityEngine.Random.Range(11,17));
                }
                if (unit.unitType.unitType == "Hoplite")
                {
                    if (UnityEngine.Random.Range(1,3) != 1)
                        audio.PlaySound(UnityEngine.Random.Range(17,20));
                }
            }
        }
    }

    public void _backToCamp()
    {
        obj = GameObject.FindGameObjectsWithTag("Base");
        seeker.StartPath(transform.position, obj[0].transform.position, _onPathComplete);
    }

    public void _findAnotherResource(String tag)
    {
        GameObject[] obe = GameObject.FindGameObjectsWithTag(tag);
        if (obe.Length == 0)
        {
            
        }
        else
        {
            seeker.StartPath(transform.position, obe[0].transform.position, _onPathComplete);   
        }
    }

    public void _backToResource(Vector3 pos)
    {
        seeker.StartPath(transform.position, pos, _onPathComplete);
    }

    public void _onPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            // Reset waypoint
            currentWaypoint = 0;
        }
    }

    public void FixedUpdate()
    {
        if (path == null) return;
        if (currentWaypoint >= path.vectorPath.Count) return;

        Vector3 direction = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        direction *= speed * Time.deltaTime;
        controller.SimpleMove(direction); // Pohyb jednotky
        
        transform.LookAt(new Vector3(path.vectorPath[currentWaypoint].x,transform.position.y,path.vectorPath[currentWaypoint].z));
        
        if (Mathf.FloorToInt(animPos.x)  -  1 == Mathf.FloorToInt(transform.position.x) || Mathf.FloorToInt(animPos.x)  +  1 == Mathf.FloorToInt(transform.position.x) || Mathf.FloorToInt(animPos.x)  == Mathf.FloorToInt(transform.position.x))
        {
            if (Mathf.FloorToInt(animPos.z)  -  1 == Mathf.FloorToInt(transform.position.z) || Mathf.FloorToInt(animPos.z)  +  1 == Mathf.FloorToInt(transform.position.z) || Mathf.FloorToInt(animPos.z)  == Mathf.FloorToInt(transform.position.z))
             anim.SetBool("isWalking", false);
        }
        // Kontrola waypointu
        if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
        {
            currentWaypoint++;
            return;
        }
    }
    
    
    private GameObject findClosestBase() {
        GameObject[] objs= GameObject.FindGameObjectsWithTag("Base");
        Debug.Log(objs.Length);
        GameObject closestEnemy = null;
        float closestDistance = 0f;
        bool first = true;
         
        foreach (var obj in objs)
        {
            Vector3 pos = obj.transform.position;
            float distance = Vector3.Distance(pos, transform.position);
            if (first)
            {
                closestDistance = distance;
                 
                first = false;
            }            
            else if (distance < closestDistance)
            {
                closestEnemy = obj;
                closestDistance = distance;
            }
                                                                         
        }
        return closestEnemy;
    }
}