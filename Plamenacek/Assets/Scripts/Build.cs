﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Build : MonoBehaviour
{
    public GameObject placeholder;
    public bool clicked = false;


    public GameObject Event;
    public GameObject farm;
    public GameObject farmPlaceholder;

    public void Start()
    {
        Event = GameObject.FindGameObjectWithTag("EventSystem");
    }
    public void _buildMain()
    {
        //Event.GetComponent<CameraFollow>().
        Event.GetComponent<CameraFollow>().enabled = true;
    }

    public void _buildFarm()
    {
        Event.GetComponent<CameraFollow>().placeholder = farmPlaceholder;
        Event.GetComponent<CameraFollow>().building = farm;
        Event.GetComponent<CameraFollow>().enabled = true;
    }


}
