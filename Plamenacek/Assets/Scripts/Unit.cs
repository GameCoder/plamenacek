﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Unit : MonoBehaviour
{
    public bool selected = false;
    public int health;
    public bool isWorker = false;
    public bool isWarrior = false;
    public bool workedFinished = false;
    public int[] stacks = new int[4];
    public int currentResource = 0;
    public int lastResource = 0;
    public UnitTypes unitType;
    private Animator anim;
    
    
    //ANIMACE
    public bool isWalking;

    
    public GameObject Event;
    public Vector3 resourcePosition;
    public Collider obj;

    public void Start()
    {
        Event = GameObject.FindGameObjectWithTag("EventSystem");
        anim = GetComponent<Animator>();
        isWalking = false;
        health = unitType.maxHealth;
        if (isWorker == true)
        {
            InvokeRepeating("_addResources", 1f, 1.0f);
        }
    }

    public void _addResources()
    {
        Debug.Log("těžím:" + currentResource);
        if (currentResource != 0)
        {
            if (stacks[currentResource] < 10)
            {
                if (currentResource == 2)
                {
                    obj.GetComponent<Tree>().health(1,this);
                }
                stacks[currentResource] = stacks[currentResource]+1;
                
                Debug.Log("Mám" + stacks[currentResource]);
            }
            else
            {
                GetComponent<UnitPath>()._backToCamp();
            }  
        }
        else
        {
            Debug.Log("Neprošlo" + currentResource);
        }
    }
    
    void OnTriggerEnter(Collider other)
    {
        obj = other;
        
        if (isWorker == true)
        {
            if (other.CompareTag("Gold"))
            {
                currentResource = 1;
                resourcePosition = other.transform.position;
            }

            if (other.CompareTag("Tree"))
            {
                Debug.Log("tree");
                currentResource = 2;
                resourcePosition = other.transform.position;
                anim.SetBool("isCutting",true);
                
            }

            if (other.CompareTag("Base"))
            {
                Debug.Log(lastResource + "base");
                switch (lastResource)
                {
                    case 1:Event.GetComponent<Player>()._setGold(stacks[lastResource]); break;
                    case 2:Event.GetComponent<Player>()._setWood(stacks[lastResource]); break;
                    default: break;
                }
                stacks[lastResource] = 0;
                currentResource = 0;
                
                anim.SetBool("isCutting",false);
                anim.SetBool("isWoodWalking",false);
                anim.SetBool("isWalking",true);

                if (workedFinished == false)
                {
                    GetComponent<UnitPath>()._backToResource(resourcePosition);
                   // anim.SetBool("isWalking",true);
                    //anim.SetBool("isWoodWalking", false);
                }
                else
                {
                    GetComponent<UnitPath>()._findAnotherResource("Tree");
                    //anim.SetBool("isWalking",true);
                    //anim.SetBool("isWoodWalking", false);
                }
                
            }
        }

        if (isWarrior == true)
        {
            if (other.CompareTag("Enemy"))
            {
                
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        lastResource = currentResource;
        anim.SetBool("isCutting",false);
        anim.SetBool("isWalking",false);
        if (lastResource == 2)
        {
            anim.SetBool("isWoodWalking",true);
        }

    }
    

}
