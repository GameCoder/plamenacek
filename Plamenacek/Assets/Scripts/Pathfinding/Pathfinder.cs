﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using UnityEngine.PlayerLoop;

public class Pathfinder : MonoBehaviour
{
    public Vector3 target;
    private Seeker seeker;
    private CharacterController controller;
    public Path path;

    public float speed;


    public float nextWaypointDistance = 10f;
    private int currentWaypoint = 0;
    
    void Start()
    {
        target = GameObject.Find("Target").transform.position;
        seeker = GetComponent<Seeker>();
        controller = GetComponent<CharacterController>();
        
        // nastavení cesty
        seeker.StartPath(transform.position,target,_onPathComplete);
    }

    public void _onPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            // Reset waypoint
            currentWaypoint = 0;
        }
    }

    public void FixedUpdate()
    {
        if (path == null) return;
        if (currentWaypoint >= path.vectorPath.Count) return;

        Vector3 direction = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        direction *= speed * Time.deltaTime;
        controller.SimpleMove(direction); // Pohyb jednotky
        
        // Kontrola waypointu
        if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
        {
            currentWaypoint++;
            return;
        }

    }
}
