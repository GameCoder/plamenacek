﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIChange : MonoBehaviour
{
    public GameObject menu;

    public Image progressBarBarracks;
    public Image progressBarMain;
   // public Button button;
    //public TextMeshProUGUI test;
    public TextMeshProUGUI name;
    public TextMeshProUGUI description;
    public TextMeshProUGUI damage;
    public TextMeshProUGUI defense;

    public Button BuildHoplite;
    public Button BuildWorker;

    public Button BuildMain;
    public Button BuildBarracks;
    public Button BuildFarm;

    public Image healthBar;
    void Start()
    {
        //menu.SetActive(false);
        // menu.SetActive(false);
        BuildHoplite.gameObject.SetActive(false);
        BuildWorker.gameObject.SetActive(false);
    }

    public void changeHealth(int health, int maxHealth)
    {
        //healthBar.fillAmount = health / maxHealth;
    }
    public void _changeUI(UnitTypes data, int [] stacks)
         {
             BuildHoplite.gameObject.SetActive(false);
             BuildWorker.gameObject.SetActive(false);
             progressBarBarracks.gameObject.SetActive(false);
             progressBarMain.gameObject.SetActive(false);
             if (data.unitType.Equals("Worker"))
             {
                 damage.text = "Gold: " + stacks[0];
                 defense.text = "Wood: " + stacks[1];
                 BuildMain.gameObject.SetActive(true);
                 BuildBarracks.gameObject.SetActive(true);
                 BuildFarm.gameObject.SetActive(true);
             }
             else
             {
                 BuildMain.gameObject.SetActive(false);
                 BuildBarracks.gameObject.SetActive(false);
                 BuildFarm.gameObject.SetActive(false);
             }

             if (data.unitType.Equals("Hoplit"))
             {
                 damage.text = data.unitDamage.ToString();
                 defense.text = data.unitDefense.ToString();
             }
             
             menu.SetActive(true);
             name.text = data.unitName;
             description.text = data.description;
             damage.text = "Damage: " + data.unitDamage.ToString();
             defense.text ="Defense: " + data.unitDefense.ToString();
         }
    public void _changeUI2(Buildings data)
    {
        BuildMain.gameObject.SetActive(false);
        BuildBarracks.gameObject.SetActive(false);
        BuildFarm.gameObject.SetActive(false);
        if (data.type.Equals("Barracks"))
        {
            BuildHoplite.gameObject.SetActive(true);
            BuildWorker.gameObject.SetActive(false);
            progressBarBarracks.gameObject.SetActive(true);
            progressBarMain.gameObject.SetActive(false);
        }

        if (data.type.Equals("MainBuilding"))
        {
            BuildHoplite.gameObject.SetActive(false);
            BuildWorker.gameObject.SetActive(true);
            progressBarMain.gameObject.SetActive(true);
            progressBarBarracks.gameObject.SetActive(false);
        }
        menu.SetActive(true);
        name.text = data.name;
        description.text = data.description;
        damage.text = "Damage: /";
        defense.text = "Defense: " + data.defense.ToString();
    }

    public void _changeResources(int gold)
    {
        //test.text = gold.ToString();
    }
}
