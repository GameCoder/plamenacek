﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Building : MonoBehaviour
{
    public bool selected = false;
    public Buildings building;
    public GameObject spawnPoint;
    private float countdown = 0f;
    private float maxCountdown;
    public Image progressBar;

    private bool trainingHoplite;
    public GameObject Hoplite;
    
    private bool trainingWorker;
    public GameObject Worker;

    private string[] front = new string[10];
    private int currentIndex;

    void Start()
    {
        currentIndex = 0;
    }

    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown > 0)
        {
            progressBar.fillAmount = countdown / maxCountdown;
        }

        if (countdown <= 0 && trainingHoplite == true)
        {
            TrainHoplit();
        }
        if (countdown <= 0 && trainingWorker == true)
        {
            TrainWorker();
        }
    }

    public void TrainHoplit()
    {
        Instantiate(Hoplite, spawnPoint.transform.position, Quaternion.identity);
        trainingHoplite = false;
    }

    public void TrainingHoplit()
    {
        Debug.Log("training");
        trainingHoplite = true;
        countdown = Hoplite.GetComponent<Unit>().unitType.buildingTime;
        maxCountdown = countdown;
    }
    public void TrainWorker()
    {
        
        Instantiate(Worker, spawnPoint.transform.position, Quaternion.identity);
        trainingWorker = false;
    }

    public void TrainingWorker()
    {
        Debug.Log("Training");
        trainingWorker = true;
        countdown = Worker.GetComponent<Unit>().unitType.buildingTime;
        maxCountdown = countdown;
    }

   /* public void AddHoplitToFront()
    {
        for (int  i = 0;  i < front.Length;  i++)
        {
            if (front[i] == null)
            {
                front[i] = "Hoplit";
                Debug.Log(front[i]);
                Debug.Log(i);
                break;
            }
           
        }
    } */
}