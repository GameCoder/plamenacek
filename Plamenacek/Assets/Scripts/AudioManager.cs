﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioClip[] clips;
    public AudioSource audio;
    
    public void PlaySound(int index)
    {
        audio.clip = clips[index];
        audio.Play();
    } 
}
