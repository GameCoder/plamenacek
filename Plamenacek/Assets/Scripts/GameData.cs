﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour
{
    public GameObject[] Units;

    public GameData(GameMaster gameMaster)
    {
        Units = gameMaster.Units;
    }
}
