﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Player : MonoBehaviour
{
    private int gold = 500;
    private int wood = 500;
    private int stone = 500;
    private int food = 500;

    public TextMeshProUGUI goldText;
    public TextMeshProUGUI woodText;
    public TextMeshProUGUI stoneText;
    public TextMeshProUGUI foodText;

    void Start()
    {
        goldText.text = "Gold: " + gold;
        woodText.text = "Wood: " + wood;
        stoneText.text = "Stone: " + stone;
        foodText.text = "Food: " + food;
    }

    public void _setGold(int set)
    {
        gold = gold + set;
        goldText.text = "Gold: " + gold;
    }

    public void _setWood(int set)
    {
        wood = wood + set;
        Debug.Log(wood);
        woodText.text = "Wood: " + wood;
    }
}
