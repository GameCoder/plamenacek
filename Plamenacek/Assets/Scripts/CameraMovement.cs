﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public static CameraMovement instance;
    public Transform followTransform;
    //pro follow u toho objektu napsat CameraMovement.instance.followTransform = transform;
    public Transform cameraTransform;
    [HideInInspector]
    public float movementSpeed;
    
    public float normalSpeed;
    public float fastSpeed;
    public float zoomSpeed;
    public float movementTime;
    public float rotationAmount;
    public float panBorderThickness;
    public Vector3 zoomAmount;

    public GameObject plane;
    [HideInInspector]
    public Vector3 newPosition;
    [HideInInspector]
    public Quaternion newRotation;
    [HideInInspector]
    public Vector3 newZoom;
        
    void Start()
    {
        newPosition = transform.position;
        newRotation = transform.rotation;
        newZoom = cameraTransform.localPosition;
    }
    
    void Update()
    {
        if (followTransform != null)
        {
            transform.position = followTransform.position;
        }
        else
        {
            HandleMouseZoom();
            HandleMovementInput();   
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            followTransform = null;
        }
    }

    void HandleMouseZoom()
    {
        if (Input.mouseScrollDelta.y != 0)
        {
            newZoom += Input.mouseScrollDelta.y * zoomAmount * zoomSpeed;
        }
    }
    void HandleMovementInput()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            movementSpeed = fastSpeed;
        }
        else
        {
            movementSpeed = normalSpeed;
        }
        if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow) || Input.mousePosition.y >= Screen.height - panBorderThickness)
        {
            newPosition += (transform.forward * movementSpeed);
        }
        if(Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow) || Input.mousePosition.y <= panBorderThickness)
        {
            newPosition += (transform.forward * -movementSpeed);
        }
        if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.LeftArrow) || Input.mousePosition.x >= Screen.width - panBorderThickness)
        {
            newPosition += (transform.right * movementSpeed);
        }
        if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.RightArrow) || Input.mousePosition.x <= panBorderThickness)
        {
            newPosition += (transform.right * -movementSpeed);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            newRotation *= Quaternion.Euler(Vector3.up * rotationAmount);
        }
        if (Input.GetKey(KeyCode.E))
        {
            newRotation *= Quaternion.Euler(Vector3.up * -rotationAmount);
        }

        if (Input.GetKey(KeyCode.Z))
        {
            newZoom += zoomAmount;
        }
        if (Input.GetKey(KeyCode.H))
        {
            newZoom -= zoomAmount;
        }
        //|| newPosition.x + 2 < transform.position.x || newPosition.z - 2 > transform.position.z || newPosition.z + 2 < transform.position.z
        if (newPosition.x - 2 > transform.position.x)
        {
            newPosition.x = newPosition.x - 2;
        }

        if (newPosition.x + 2 < transform.position.x)
        {
            newPosition.x = newPosition.x + 2;
        }

        if (newPosition.z - 2 > transform.position.z)
        {
            newPosition.z = newPosition.z - 2;
        }

        if (newPosition.z + 2 < transform.position.z)
        {
            newPosition.z = newPosition.z + 2;
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, newPosition, Time.deltaTime * movementTime);   
        }
        transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, Time.deltaTime * movementTime);
        cameraTransform.localPosition = Vector3.Lerp(cameraTransform.localPosition, newZoom, Time.deltaTime * movementTime);
    }
}