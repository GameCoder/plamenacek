﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BuildingType", menuName = "Building")]
public class Buildings : ScriptableObject
{

    public string name;
    public string type;
    public string maxhealth;
    public string defense;
    public string description;
    public int[] cost;
    
}
