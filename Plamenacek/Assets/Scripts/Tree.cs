﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    public int treeResources = 10;
    public Animator anim;
    public void health(int minus,Unit u)
    {
        treeResources -= minus;
        if (treeResources <= 0)
        {
            Debug.Log(treeResources + "weae");
            anim.SetBool("Destroy",true);
            u.workedFinished = true;
            
        }
    }

    public void Update()
    {
        if(anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
        {
            string name = transform.parent.GetChild(0).gameObject.name;
            if (name == "koruna" || name == "_box")
            {
                Destroy (transform.parent.GetChild(0).gameObject);
                Destroy(transform.parent.GetChild(2).gameObject);
            }
        }
    }
}
