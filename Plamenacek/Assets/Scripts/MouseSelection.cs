﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseSelection : MonoBehaviour
{
    public static int workName = 0;
    public static Vector3 rightClickPosition;
    private UIChange ui;
    public static ArrayList CurrentlySelectedUnits = new ArrayList();
    public AudioManager audio;

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "Gold")
                {
                    workName = 1;
                }

                if (!hit.transform.CompareTag("Friedly"))
                {
                    rightClickPosition = hit.point;    
                }
                
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
           
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "Friedly")
                {
                    ui = GetComponent<UIChange>();
                    ui._changeUI(hit.transform.gameObject.GetComponent<Unit>().unitType, hit.transform.gameObject.GetComponent<Unit>().stacks);
                    DeselectSelectedObjects();
                    hit.transform.gameObject.GetComponent<Unit>().selected = true;
                    ui.changeHealth(hit.transform.gameObject.GetComponent<Unit>().health, hit.transform.gameObject.GetComponent<Unit>().unitType.maxHealth);
                    //Zvuky
                    if (hit.transform.gameObject.GetComponent<Unit>().unitType.unitType == "Worker")
                    {
                        if (UnityEngine.Random.Range(1,3) != 1)
                            audio.PlaySound(Random.Range(0,4));
                    }
                    if (hit.transform.gameObject.GetComponent<Unit>().unitType.unitType == "Hoplite")
                    {
                        if (UnityEngine.Random.Range(1,3) != 1)
                            audio.PlaySound(Random.Range(4,11));
                    }
                    
                       
                }

                if (hit.transform.tag == "Friendly_Building")
                {
                    ui = GetComponent<UIChange>();
                    ui._changeUI2(hit.transform.gameObject.GetComponent<Building>().building);
                    DeselectSelectedObjects();
                    hit.transform.gameObject.GetComponent<Building>().selected = true;
                }
            }
        }

        if (Input.GetKeyDown("left shift"))
        {
            DeselectSelectedObjects();
        }
    }

    public static void DeselectSelectedObjects()
    {
        foreach (GameObject unitOBJ in GameObject.FindGameObjectsWithTag("Friedly"))
        {
            unitOBJ.GetComponent<Unit>().selected = false;
        }
    }
}