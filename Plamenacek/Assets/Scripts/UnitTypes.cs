﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UnitType", menuName = "Unit")]
public class UnitTypes : ScriptableObject
{
    public string unitName;
    public string unitType;
    public int unitDamage;
    public int unitDefense;
    public bool ranged;
    public int  maxHealth;
    public int[] cost;
    public string description;
    public float buildingTime;
}
